import { Injectable } from '@nestjs/common';
import { CreateWordDto } from './dto/create-word.dto';
import { UpdateWordDto } from './dto/update-word.dto';
import {Word} from "./entities/word.entity";
import {UsersService} from "../users/users.service";

@Injectable()
export class WordsService {
  constructor(private userService: UsersService) {
  }
  // save a note
  async create(createWordDto: CreateWordDto, headers) {
    const id = await this.userService.validateUser(headers.authorization);
    return Word.save({...createWordDto,userId: id, like: false});
  }

  async findByUserId(headers) {
    const id = await this.userService.validateUser(headers.authorization);

    return  await Word.find({
      where: {
        userId: id,
      },
    });
  }

  // Update note using note ID
  async update(id: number, updateWordDto: UpdateWordDto) {
    const property = await Word.findOne({
      where: { id }
    });
    updateWordDto.like = !property.like
    return Word.save({
      ...property, // existing fields
      ...updateWordDto // updated fields
    });
  }

  // Delete a note
  remove(id: number) {
    return Word.delete(id);
  }
}
