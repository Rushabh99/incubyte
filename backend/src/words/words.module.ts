import { Module } from '@nestjs/common';
import { WordsService } from './words.service';
import { WordsController } from './words.controller';
import {PassportModule} from "@nestjs/passport";
import {AuthService} from "../auth/auth.service";
import {UsersService} from "../users/users.service";
import {JwtModule} from "@nestjs/jwt";
import {JwtStrategy} from "../authentication/jwt.strategy";

import {ConfigModule, ConfigService} from "@nestjs/config";

@Module({
  imports: [
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'user',
      session: false,
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async () => ({
        secret: process.env.JWT_SECRET,
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [WordsController],
  providers: [WordsService, AuthService, UsersService, JwtStrategy]
})
export class WordsModule {}
