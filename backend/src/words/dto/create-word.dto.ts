import {IsNotEmpty, IsNumber} from "class-validator";

export class CreateWordDto {
    @IsNotEmpty()
    word: string;

    like: boolean;
}
