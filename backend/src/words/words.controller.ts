import {Controller, Get, Post, Body, Param, Delete, UseGuards} from '@nestjs/common';
import { WordsService } from './words.service';
import {CreateWordDto} from "./dto/create-word.dto";
import {UpdateWordDto} from "./dto/update-word.dto";
import {AuthGuard} from "@nestjs/passport";
import { Headers } from '@nestjs/common';

@Controller('words')
export class WordsController {
  constructor(private readonly wordsService: WordsService) {}

  @Post()
  @UseGuards(AuthGuard())
  create(@Body() createWordDto: CreateWordDto,  @Headers() headers) {
    return this.wordsService.create(createWordDto, headers);
  }


  @Get()
  @UseGuards(AuthGuard())
  findByUser( @Headers() headers) {
    return this.wordsService.findByUserId(headers);
  }

  @Post(':id')
  @UseGuards(AuthGuard())
  update(@Param('id') id: number, @Body() updateWordDto: UpdateWordDto) {
    return this.wordsService.update(id, updateWordDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  remove(@Param('id') id: string) {
    return this.wordsService.remove(+id);
  }
}
