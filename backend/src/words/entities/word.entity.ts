import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity, IsNull, ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import {User} from "../../users/entities/user.entity";

@Entity()
export class Word extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    word: string;

    @Column()
    like: boolean;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    userId: number;

    @ManyToOne(() => User, (user) => user.words, {
        eager: false,
        onDelete: 'CASCADE',
    })
    user: User[];
}
