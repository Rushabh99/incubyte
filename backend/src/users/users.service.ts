import { Injectable } from '@nestjs/common';

import { CreateUserDto } from './dto/create-user.dto';
import {User} from "./entities/user.entity";
import {JwtService} from "@nestjs/jwt";

@Injectable()
export class UsersService {
  constructor(private jwtService: JwtService,
              ) {
  }

  async validateUser(token: string){
    if(token.includes("Bearer "))token = token.split(" ")[1]
     const user = await User.findOne({
       where: {
         token,
       },
     })
      return user ? user.id : 0;
  }

  async create(createUserDto: CreateUserDto) {
    const user = User.create(createUserDto);
    const token = this.jwtService.sign({
      userId: user.id,
    })
    user.token = token
    await user.save();

    return {
      access_token: token,
    };  }

  async showById(id: number): Promise<User> {
    const user = await this.findById(id);

    delete user.password;
    return user;
  }

  async findById(id: number) {
    return await User.findOne({
      where: {
        id: id,
      },
    });
  }

  async findByEmail(email: string) {
    return await User.findOne({
      where: {
        email: email,
      },
    });
  }
}
