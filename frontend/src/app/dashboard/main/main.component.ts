import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataService} from "../../services/data.service";
import {QueryService} from "../../services/query.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public wordList: any = [];
  breakpoint: number | undefined;
  public wordText: string="";

  constructor(
    private dialog: MatDialog,
    private dataService: DataService,
    private queryService: QueryService
  ) { }

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 400) ? 1 : 4;
    this.getList()
  }


  onResize(event: any) {
    if(event.target.innerWidth <= 1100)this.breakpoint = 4
    if(event.target.innerWidth <= 980)this.breakpoint = 3
    if(event.target.innerWidth <= 730)this.breakpoint = 2
    if(event.target.innerWidth <= 476)this.breakpoint = 1
  }

  getList() {
    this.dataService.get(this.queryService.getUrl("getList")).subscribe((data)=>{
      this.wordList = data;
    })
  }

  like(id: number) {
    this.dataService.post(this.queryService.getUrl("updateList")+id,{like: true}).subscribe((data)=>{
      this.getList()
    })
  }

  delete(id: number) {
    this.dataService.delete(this.queryService.getUrl("deleteList")+id,{like: true}).subscribe((data)=>{
      this.getList()
    })
  }

  save() {
    this.dataService.post(this.queryService.getUrl("createList"),{word: this.wordText}).subscribe((data)=>{
      this.wordText = "";
      this.dialog.closeAll()
      this.getList()
    })
  }

  @ViewChild('secondDialog', { static: true }) secondDialog: TemplateRef<any> | undefined;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
}
