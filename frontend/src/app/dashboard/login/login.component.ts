import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {first} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationservice: AuthService,
  ) { }
  ngOnInit() {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.authenticationservice.logout();
  }

  get f() { return this.form.controls; }

  login() {
    this.submitted = true;
    if (this.form && this.form.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationservice.login({email: this.f['email'].value, password: this.f['password'].value})
      .pipe(first())
      .subscribe(
        (data: any) => {
          sessionStorage.setItem('currentUser',data['access_token']);
        },
        error => {
          this.loading = false;
        });
}
  signup() {
    this.submitted = true;
    if (this.form && this.form.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationservice.signup({email: this.f['email'].value, password: this.f['password'].value})
      .pipe(first())
      .subscribe(
        (data: any) => {
          sessionStorage.setItem('currentUser',data['access_token']);
        },
        error => {
          this.loading = false;
        });
  }
}
