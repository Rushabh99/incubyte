import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  public queries = {
    getList: "words",
    updateList: "words/",
    createList: "words",
    login: "auth",
    signup: "users",
    deleteList: "words/",
  }
  constructor() { }

  getUrl(apiName: string) {
    const keyIndex = Object.keys(this.queries).indexOf(apiName)
    return environment.baseUrl + Object.values(this.queries)[keyIndex]
  }
}
