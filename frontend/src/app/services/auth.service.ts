import { Injectable } from '@angular/core';
import {DataService} from "./data.service";
import {QueryService} from "./query.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

constructor(private dataservice: DataService,
            private queryService: QueryService) {
}

  login(data: {email: string, password: string}) {
    return this.dataservice.post(this.queryService.getUrl("login"), data)
  }

  signup(data: {email: string, password: string}) {
    return this.dataservice.post(this.queryService.getUrl("signup"), data)
  }

  logout() {
    sessionStorage.removeItem('currentUser');
  }
}
